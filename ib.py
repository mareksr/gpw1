import urllib.request
import zipfile
import os
import numpy as np
import pandas as pd
import math
    



#https://thispointer.com/python-how-to-unzip-a-file-extract-single-multiple-or-all-files-from-a-zip-archive/

STOCKS_DIR='d:\stocks'

def atr(df, n):
    """
    
    :param df: pandas.DataFrame
    :param n: 
    :return: pandas.DataFrame
    """
    i = 0
    TR_l = [0]
    while i < df.index[-1]:
        TR = max(df.loc[i + 1, 'HIGH'], df.loc[i, 'CLOSE']) - min(df.loc[i + 1, 'LOW'], df.loc[i, 'CLOSE'])
        #TR = max(df.loc[i+1, 'HIGH']-df.loc[i+1, 'LOW'], math.fabs(df.loc[i+1, 'HIGH']-df.loc[i, 'CLOSE']),
        #         math.fabs(df.loc[i+1, 'LOW']-df.loc[i, 'CLOSE']))
        TR_l.append(TR)
        i = i + 1
    TR_s = pd.Series(TR_l)
    ATR = pd.Series(TR_s.ewm(span=n, min_periods=n).mean(), name='ATR_' + str(n))
    df = df.join(ATR)
    return df

"""
roznica w wielkosci bar zew i bar wew po HIGH/LOW
"""
def size(df):
    i=0
    TR_l = [0]
    while i < df.index[-1]:
      TR= ((df.loc[i, 'HIGH']-df.loc[i, 'LOW']) - (df.loc[i + 1, 'HIGH']-df.loc[i+1, 'LOW']))
      TR_l.append(TR)
      i = i + 1
    TR_s = pd.Series(TR_l,name="DIFF")
    df = df.join(TR_s)
    return df

def downloadstocks():
    print('Beginning file download with mstall.zip...')
    url = 'http://bossa.pl/pub/metastock/mstock/mstall.zip'
    urllib.request.urlretrieve(url, 'd:\mstall.zip')
    print('Done... Unzipping d:/stocks')
    
def unpackstocks(stocklist):
  with zipfile.ZipFile("d:\mstall.zip","r") as zip_ref:
    for stock in stocklist:
      zip_ref.extract(stock, STOCKS_DIR)
  print('unpacking done')

    
def check(stocklist,date):
  for stock in stocklist:
  #df = pd.read_csv('d:\stocks\%s' %(stock),  names=['TICKER','DATA', 'OPEN', 'HIGH', 'LOW', 'CLOSE', 'VOL'], index_col=1, skiprows=1, parse_dates=['DATA'])
  
    df = pd.read_csv('d:\stocks\%s' %(stock),  names=['TICKER','DATA', 'OPEN', 'HIGH', 'LOW', 'CLOSE', 'VOL'],  skiprows=1, parse_dates=['DATA'])
    df.dtypes
    df['vol10'] = df.VOL.rolling(10).mean()
    df['EMA_200'] = df['CLOSE'].ewm(span=200,min_periods=0,adjust=False,ignore_na=False).mean()
    x=df['VOL'].rolling(10).mean()
    #x
    #df.set_index('DATA').join(x.set_index('DATA'))
    df['columnX']=np.sign(df.HIGH.diff().fillna(0)) # -1 gdy ROW[n+1]<ROW[n] , 1 gdy ROW[n+1]>ROW[n]
    df['columnY']=np.sign(df.LOW.diff().fillna(0))
    df=atr(df,20)
    df=size(df)
    df['k']=(df['HIGH']-df['LOW'])/df['ATR_20']

  
    X=df['columnX']<=0 
    Y=df['columnY']>=0
    EMA=df['EMA_200']<df['CLOSE']
    AR=df['k']<0.523

    df[X & Y]
    p=df[X & Y & EMA & AR]
    q=(p[-1:])
    z=q['DATA']==date
    if not q[z].empty:  
      t=(q[z])
    
    #relacja bara wewnetrznego do zmiennosci atr(20)
     
      
      
      #print(t.filter(items=['TICKER','ATR_20','HIGH', 'e',  'CLOSE','ewm']).to_string(header=False))
      print(t.filter(items=['TICKER','ATR_20','EMA_200','DIFF','k']).to_string(header=False))


#zmienić na ładowanie z pliku

date = pd.Timestamp("today").strftime("%Y-%m-%d")

wig20 = np.loadtxt("d:/stocks/wig20.txt", comments="#", delimiter=",", unpack=False,dtype=str)
mwig40 = np.loadtxt("d:/stocks/mwig40.txt", comments="#", delimiter=",", unpack=False,dtype=str)

#downloadstocks()
unpackstocks(wig20)
unpackstocks(mwig40)

print("WIG20")
check(wig20,"2019-11-20")
#check(wig20,date)
print("mWIG40")
#check(mwig40,date)
check(mwig40,"2019-11-20")


